<?php
/**
 * Render your site front page, whether the front page displays the blog posts index or a static page.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#front-page-display
 *
 * @package wprig
 */

get_header();

/*
* Include the component stylesheet for the content.
* This call runs only once on index and archive pages.
* At some point, override functionality should be built in similar to the template part below.
*/
?>
	<main id="primary" class="site-main">
		<?php
		while ( have_posts() ) :
			the_post();

			/*
			* Services
			*/
			$services = get_field( 'services' );
			if ( $services ) {
				$args = [
					'post_type' => 'services',
					'post__in'  => $services,
					'orderby'   => 'post__in',
				];
				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) {
					echo '<div class="services"><div class="services-container"><h2>Services</h2>';
					while ( $the_query->have_posts() ) {
						$the_query->the_post();

						get_template_part( 'template-parts/services' );
					}
					echo '<div class="button-container"><a class="button" href="/services/">View All</a></div></div></div>';
				}

				wp_reset_postdata();
			}

			/*
			* News
			*/
			$posts = get_field( 'news' );
			if ( $posts ) {
				$args = [
					'post__in' => $posts,
					'orderby'   => 'post__in',
				];
				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) {
					echo '<div class="news"><h2>News</h2>';

					while ( $the_query->have_posts() ) {
						$the_query->the_post();

						get_template_part( 'template-parts/news' );
					}

					echo '</div>';
				}

				wp_reset_postdata();
			}

			/*
			* Call to action
			*/
			$cta = get_field( 'call_to_action' );
			if ( ! empty( $cta ) ) :
				?>
				<div class="cta-container">
					<div id="homepage-cta"></div>
					<div class="homepage-cta-container">
					<p><?php echo esc_html( $cta['description'] ); ?></p>
					<div class="button-container">
						<a href="<?php echo esc_url( $cta['button']['url'] ); ?>" class="button"><?php echo esc_html( $cta['button']['title'] ); ?></a>
					</div>
				</div>
			</div>

				<?php
				printf( '<style>#homepage-cta { background-image: url( %s ) }</style>', esc_url( $cta['background'] ) );
			endif;
		endwhile; // End of the loop.
		?>

	</main><!-- #primary -->

<?php
get_footer();

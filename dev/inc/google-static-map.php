<?php
/**
 * Google Static Map
 *
 * @package wprig
 */

function gstaticmap_function( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'w'        => '480',
			'h'        => '480',
			'z'        => '17',
			'col'      => 'red',
			'type'     => 'roadmap',
			'addr'     => 'Oklahoma City, OK',
			'link'     => 'false',
			'divclass' => '',
			'scale'    => '1',
		),
		$atts
	);

	$static_map_url = add_query_arg(
		array(
			'center'  => urlencode( $atts['addr'] ),
			'zoom'    => $atts['z'],
			'size'    => $atts['w'] . 'x' . $atts['h'], // returns 1280x700 at free API level.
			'maptype' => $atts['type'],
			'markers' => 'color:' . $atts['col'] . '|' . urlencode( $atts['addr'] ) . '|label:m',
			'key'     => 'AIzaSyC20QYd9qWv2kCt-wto9Y-Dg907dy5NHLU',
			'scale'   => $atts['scale'],
			'style'   => 'feature:poi|visibility:off',
			'format'  => 'png8',
		),
		'https://maps.googleapis.com/maps/api/staticmap'
	);
	$static_map_url = esc_url_raw( $static_map_url, 'https' );

	$map = sprintf(
		'<img alt="%s" src="%s" />',
		$atts['addr'],
		$static_map_url
	);

	if ( 'true' === $atts['link'] ) {
		$url = sprintf(
			'http://maps.google.com/maps?q=%s',
			$atts['addr']
		);

		$map = sprintf(
			'<a href="%s">%s</a>',
			$url,
			$map
		);
	}

	if ( ! empty( $atts['divclass'] ) ) {
		$map = sprintf(
			'<div class="%s">%s</div>',
			$atts['divclass'],
			$map
		);
	}

	return $map;
}

add_shortcode( 'gstaticmap', 'gstaticmap_function' );
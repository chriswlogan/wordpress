<?php
/**
 * Post Type definitions
 *
 * @package wprig
 */

function cptui_register_my_cpts() {

	/**
	 * Post Type: Doctors.
	 */

	$labels = [
		'name' => __( 'Doctors', 'wprig' ),
		'singular_name' => __( 'Doctor', 'wprig' ),
	];

	$args = [
		'label'               => __( 'Doctors', 'wprig' ),
		'labels'              => $labels,
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_rest'        => true,
		'rest_base'           => '',
		'has_archive'         => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'map_meta_cap'        => true,
		'hierarchical'        => false,
		'rewrite'             => [ 'slug' => 'doctors', 'with_front' => false, 'feeds' => false ],
		'query_var'           => true,
		'menu_icon'           => 'dashicons-id',
		'supports'            => [ 'title', 'editor', 'thumbnail', 'excerpt' ],
	];

	register_post_type( 'doctors', $args );

	/**
	 * Post Type: Services.
	 */

	$labels                =  [
		'name'                => __( 'Services', 'wprig' ),
		'singular_name'       => __( 'Service', 'wprig' ),
	];

	$args                  =  [
		'label'               => __( 'Services', 'wprig' ),
		'labels'              => $labels,
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_rest'        => true,
		'rest_base'           => '',
		'has_archive'         => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'map_meta_cap'        => true,
		'hierarchical'        => false,
		'rewrite'             => [ 'slug' => 'services', 'with_front' => false, 'feeds' => false ],
		'query_var'           => true,
		'menu_icon'           => 'dashicons-networking',
		'supports'            => [ 'title', 'editor', 'thumbnail', 'excerpt' ],
	];

	register_post_type( 'services', $args );
}

add_action( 'init', 'cptui_register_my_cpts' );

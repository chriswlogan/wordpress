<?php
/**
 * Template Name: Services
 *
 * @package wprig
 */

get_header();

?>
	<main id="primary" class="site-main">
		<?php
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/content', get_post_type() );

			/*
			* Services
			*/
			$args = [
				'post_type' => 'services',
				'orderby'   => 'menu_order',
				'posts_per_page' => '30',
				'order' => 'ASC'
			];
			$the_query = new WP_Query( $args );
			if ( $the_query->have_posts() ) {
				echo '<div class="services"><div class="services-container"><h2>Services</h2>';

				while ( $the_query->have_posts() ) {
					$the_query->the_post();

					get_template_part( 'template-parts/services' );
				}
				echo '</div></div>';


				wp_reset_postdata();
			}

			echo get_field( 'bottom' );

		endwhile; // End of the loop.
		?>

	</main><!-- #primary -->

<?php
get_footer();

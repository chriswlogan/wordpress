<?php
/**
 * Template Name: Doctors
 *
 * @package wprig

 */

get_header();

wp_print_styles( array( 'wprig-doctors' ) );

?>
	<main id="primary" class="site-main">
		<?php
		while ( have_posts() ) :
			the_post();


			/*
			* Services
			*/
			$args = [
				'post_type' => 'doctors',
				'orderby'   => 'menu_order',
			];
			$the_query = new WP_Query( $args );
			if ( $the_query->have_posts() ) {
				echo '<div class="doctors"><div class="doctors-container">';

				while ( $the_query->have_posts() ) {
					$the_query->the_post();

					get_template_part( 'template-parts/doctors' );
				}
				echo '</div></div>';


				wp_reset_postdata();
			}
		endwhile; // End of the loop.
		?>

	</main><!-- #primary -->

<?php
get_footer();

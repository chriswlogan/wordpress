<?php
/**
 * Template Name: Insurance
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wprig
 */

get_header(); ?>

	<main id="primary" class="site-main">

	<?php
	wp_print_styles( array( 'wprig-content' ) );

	/* Start the Loop. */
	while ( have_posts() ) :
		the_post();

		the_content();

		$ins_types = get_field( 'insurance_types' );
		foreach ( $ins_types as $ins_type ) {
			if ( ! empty( $ins_type['insurance'] ) ) {
				printf( '<h2>%s</h2><ul>', esc_html( $ins_type['type'] ) );

				foreach ( $ins_type['insurance'] as $ins ) {
					printf( '<li><a href="%s" alt="%s" title="%s" target="%s">%s</a></li>',
						esc_url( $ins['link']['url'] ),
						esc_attr( $ins['link']['title'] ),
						esc_attr( $ins['link']['title'] ),
						esc_attr( $ins['link']['target'] ),
						wp_get_attachment_image( $ins['image'] )
					);
				}

				echo '</ul>';
			}
		}
	endwhile;
	?>

	</main><!-- #primary -->

<?php
get_footer();

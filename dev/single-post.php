<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package wprig
 */

get_header(); ?>

	<main id="primary" class="site-main">

		<?php

		while ( have_posts() ) :
			the_post();
		?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<?php
				if ( is_singular() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
				else :
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif;
				?>
			</header><!-- .entry-header -->

			<?php the_post_thumbnail('large'); ?>

			<div class="entry-content">
				<?php
				the_content(
					sprintf(
						wp_kses(
							/* translators: %s: Name of current post. Only visible to screen readers */
							__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'wprig' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					)
				);
				wp_link_pages(
					array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wprig' ),
						'after'  => '</div>',
					)
				);
				?>
		</article><!-- #post-<?php the_ID(); ?> -->

		<!-- Post Navigation-->
		<div class="post-nav">
			<?php
			the_post_navigation(
			array(
				'prev_text' => '<span>' . esc_html__( '<< Previous Post', 'wprig' ) . '</span>',
				'next_text' => '<span>' . esc_html__( 'Next Post >>', 'wprig' ) . '</span>',
					)
			);
			?>
		</div>
		<!--End Post Navigation -->

		<?php
		endwhile; // End of the loop.
		?>

	</main><!-- #primary -->

<?php
get_footer();

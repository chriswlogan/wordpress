<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package wprig
 */

get_header();?>

	<main id="primary" class="single-doctors">

		<?php
		wp_print_styles( array( 'wprig-doctors', 'wprig-content' ) );

		while ( have_posts() ) :
			the_post();
		?>

			<aside>
				<?php the_post_thumbnail('doctors-crop'); ?>

				<div class="doctor-position">
					<?php echo esc_html( get_field( 'position' ) ); ?>
				</div>

				<div class="doctor-bio">
					<?php echo wp_kses_post( get_field( 'bio' ) ); ?>
				</div>
			</aside>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<?php
				if ( is_singular() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
				else :
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif;
				?>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php
				the_content(
					sprintf(
						wp_kses(
							/* translators: %s: Name of current post. Only visible to screen readers */
							__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'wprig' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					)
				);
				?>

				<?php

				echo '<div class="bottom">' . wp_kses_post( get_field( 'bottom' ) ) . '</div>';

				$rows = get_field( 'awards' );
				if ( $rows ) {
					echo '<h2>Awards</h2><ul>';

					foreach ( $rows AS $row ) {
						printf( '<li>%s</li>', esc_html( $row['award'] ) );
					}

					echo '</ul>';
				}

				$rows = get_field( 'memberships' );
				if ( $rows ) {
					echo '<h2>Memberships</h2><ul>';

					foreach ( $rows AS $row ) {
						printf( '<li>%s</li>', esc_html( $row['membership'] ) );
					}

					echo '</ul>';
				}

				?>

			</div><!-- .entry-content -->
		</article><!-- #post-<?php the_ID(); ?> -->

		<?php

				/*
				* Services
				*/
				$services = get_field( 'services' );
				if ( $services ) {
					$args = [
						'post_type' => 'services',
						'post__in'  => $services,
						'orderby'   => 'post__in',
					];
					$the_query = new WP_Query( $args );
					if ( $the_query->have_posts() ) {
						echo '<div class="services"><div class="services-container"><h2>Services</h2>';
						while ( $the_query->have_posts() ) {
							$the_query->the_post();

							get_template_part( 'template-parts/services' );
						}
						echo '</div></div>';
					}

					wp_reset_postdata();
				}
				?>


		<?php
		endwhile; // End of the loop.
		?>

	</main><!-- #primary -->

<?php
get_footer();

'use strict';

module.exports = {
	theme: {
		slug: 'walnuthill',
		name: 'Walnut Hill',
		author: 'BigWing'
	},
	dev: {
		browserSync: {
			live: false,
			proxyURL: 'walnuthill2018.local:80',
			bypassPort: '8181'
		},
		browserslist: [ // See https://github.com/browserslist/browserslist
			'> 1%',
			'last 2 versions'
		],
		debug: {
			styles: false, // Render verbose CSS for debugging.
			scripts: false // Render verbose JS for debugging.
		}
	},
	export: {
		compress: true
	}
};

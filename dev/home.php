<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#home
 *
 * @package wprig
 */

get_header(); ?>

	<main id="primary" class="site-main">
<div class="article-container">
		<?php
		while ( have_posts() ) :
			the_post();

			/*
			* Include the component stylesheet for the content.
			* This call runs only once on index and archive pages.
			* At some point, override functionality should be built in similar to the template part below.
			*/
			wp_print_styles( array( 'wprig-content', 'wprig-forms' ) ); // Note: If this was already done it will be skipped.

			get_template_part( 'template-parts/content', get_post_type() );

		endwhile; // End of the loop.
		?>
	</div>
		<?php
		get_sidebar();
		?>
	</main><!-- #primary -->

<?php
get_footer();

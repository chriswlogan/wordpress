<?php
/**
 * Services module
 *
 * @package WP_Rig
 */

$heading = get_field( 'hero_heading' );
if ( ! $heading ) {
	$heading = get_the_title();
}
?>

<div class="service">
	<h3><?php echo esc_html( $heading ); ?></h3>
	<?php
	$icon = get_field( 'icon', get_the_ID() );
	if ( $icon ) {
		echo wp_get_attachment_image( $icon, 'thumbnail' );
	}
	?>
	<?php the_excerpt(); ?>
	<a class="learn-more" href="<?php the_permalink(); ?>">Learn More <span>&#43;</span></a>
</div>

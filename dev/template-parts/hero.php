<?php
/**
 * Hero module
 *
 * @package WP_Rig
 */

$hero_heading = get_field( 'hero_heading' );

if ( empty( $hero_heading ) ) {
	$hero_heading = get_the_title( $post_id );
} else {
	$hero_heading_array = explode( "\n", $hero_heading );
	if ( ! empty( $hero_heading_array[1] ) ) {
		$hero_heading = sprintf( '%s<span>%s</span>',
			esc_html( $hero_heading_array[0] ),
			esc_html( $hero_heading_array[1] )
		);
	}
}

if ( 'post' === get_post_type() ) {
	$hero_heading = 'Blog';
}

wp_print_styles( 'wprig-hero' );
?>
<div class="hero">
  <div class="hero_container">
	<h1><?php echo wp_kses_post( $hero_heading ); ?></h1>
	<?php
	$hero_description = get_field( 'hero_description' );
	if ( ! empty( $hero_description ) ) {
		printf( '<p>%s</p>', esc_html( $hero_description ) );
	}
	?>
	<?php
	$hero_button = get_field( 'hero_button' );
	if ( ! empty( $hero_button ) ) {
		printf( '<a class="button" href="%s">%s</a>',
			esc_url( $hero_button['url'] ),
			esc_html( $hero_button['title'] )
		);
	}
	?>
	</div>
</div>

<?php
	$hero_background = get_field( 'hero_background' );
	if ( ! empty( $hero_background ) ) :
?>

<style>
	.hero {
		background-image: url(<?php echo esc_url( $hero_background ); ?>);
	}
</style>

<?php
endif;

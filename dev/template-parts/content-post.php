<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wprig
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php wprig_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php
		endif;
		?>
	</header><!-- .entry-header -->

	<?php the_post_thumbnail('medium_large'); ?>

	<div class="entry-content">
		<?php the_excerpt(); ?>
		<a class="learn-more" href="<?php the_permalink(); ?>">Read More <span>&#187;</span></a>
	</div><!-- .entry-content -->

	<footer class="entry-footer"></footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->

<?php

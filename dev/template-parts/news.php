<?php
/**
 * News module
 *
 * @package WP_Rig
 */

?>

<div class="post">
	<a href="<?php the_permalink(); ?>">
	<?php
	if ( has_post_thumbnail() ) {
		the_post_thumbnail( 'medium' );
	}
	?>
	<div class="news-content-container">
		<h3><?php the_title(); ?></h3>
	</a>
			<?php the_excerpt(); ?>
	</div>
	<a class="learn-more" href="<?php the_permalink(); ?>">Learn More</a>
</div>

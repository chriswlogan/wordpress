<?php
/**
 * Services module
 *
 * @package WP_Rig

 */

?>

<div class="doctor">
	<a href="<?php the_permalink(); ?>">
		<?php the_post_thumbnail( 'doctors-crop' ); ?>
		<h3><?php echo esc_html( get_the_title() ); ?></h3>
		<h4><?php echo esc_html( get_field( 'position' ) ); ?></h4>
	</a>
</div>

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wprig
 */

?>

<footer id="colophon" class="site-footer">
	<div class="site-footer-container">
		<div class="footer-nav-container">
		<h3>Services</h3>
		<ul>
			<?php
				$args = [
					'posts_per_page' => 20,
					'post_type'      => 'services',
					'post_status'    => 'publish',
					'orderby'        => 'title',
					'order'          => 'ASC',
				];

				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) {
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						printf( '<li><a href="%s">%s</a></li>',
							esc_url( get_permalink() ),
							esc_html( get_the_title() )
						);
					}
				}
			?>
		</ul>
		</div>
	<div class="footer-nav-container about">
	<h3>About</h3>
	<?php
        wp_nav_menu( [
			'menu_id' => 'footer',
		] );
	?>
	</div>

	<div class="footer-nav-container footer-contact-us">
		<?php dynamic_sidebar( 'footer' ); ?>
	</div>
		</div>
	<div class="site-info">
	<div class="socials">
				<?php
					wp_nav_menu( [
						'theme_location' => 'social-menu',
					] );
				?>
	</div>
	<div class="copy-right">
		&copy; <?php echo date( 'Y' ); ?> Walnut Hill - All Rights Reserved
		<span class="sep"> | </span>
		<a href="<?php echo esc_url( __( '/privacy-policy/', 'walnuthill2018' ) ); ?>">Privacy Policy</a>
	</div>
	</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

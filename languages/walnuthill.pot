# Copyright (C) 2018 Walnut Hill
# This file is distributed under the same license as the Walnut Hill package.
msgid ""
msgstr ""
"Project-Id-Version: Walnut Hill\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Last-Translator: BigWing\n"
"Report-Msgid-Bugs-To: Walnut Hill\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: 404.php:16
msgid "Oops! That page can&rsquo;t be found."
msgstr ""

#: 404.php:20
msgid "It looks like nothing was found at this location. Maybe try one of the links below or a search?"
msgstr ""

#: 404.php:29
msgid "Most Used Categories"
msgstr ""

#. translators: %1$s: smiley
#: 404.php:48
msgid "Try looking in the monthly archives. %1$s"
msgstr ""

#. translators: 1: title.
#: comments.php:36
msgid "One thought on &ldquo;%1$s&rdquo;"
msgstr ""

#. translators: 1: comment count number, 2: title.
#: comments.php:42
msgctxt "comments title"
msgid "%1$s thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: comments.php:85
msgid "New comment(s)"
msgstr ""

#: comments.php:94
msgid "Comments are closed."
msgstr ""

#: functions.php:50
msgid "Primary"
msgstr ""

#: functions.php:51
msgid "Footer"
msgstr ""

#: functions.php:110
msgid "Dusty orange"
msgstr ""

#: functions.php:115
msgid "Dusty red"
msgstr ""

#: functions.php:120
msgid "Dusty wine"
msgstr ""

#: functions.php:125
msgid "Dark sunset"
msgstr ""

#: functions.php:130
msgid "Almost black"
msgstr ""

#: functions.php:135
msgid "Dusty water"
msgstr ""

#: functions.php:140
msgid "Dusty sky"
msgstr ""

#: functions.php:145
msgid "Dusty daylight"
msgstr ""

#: functions.php:150
msgid "Dusty sun"
msgstr ""

#: functions.php:171
msgid "small"
msgstr ""

#: functions.php:172
msgid "S"
msgstr ""

#: functions.php:177
msgid "regular"
msgstr ""

#: functions.php:178
msgid "M"
msgstr ""

#: functions.php:183
msgid "large"
msgstr ""

#: functions.php:184
msgid "L"
msgstr ""

#: functions.php:189
msgid "larger"
msgstr ""

#: functions.php:190
msgid "XL"
msgstr ""

#: functions.php:232
msgctxt "Playfair Display font: on or off"
msgid "on"
msgstr ""

#: functions.php:236
msgctxt "Open Sans font: on or off"
msgid "on"
msgstr ""

#: functions.php:301
msgid "Sidebar"
msgstr ""

#: functions.php:303
msgid "Add widgets here."
msgstr ""

#: functions.php:377
msgid "Expand child menu"
msgstr ""

#: functions.php:378
msgid "Collapse child menu"
msgstr ""

#: header.php:29
msgid "Skip to content"
msgstr ""

#: header.php:56
msgid "Main menu"
msgstr ""

#: header.php:71
msgid "Open menu"
msgstr ""

#: header.php:77
msgid "Menu"
msgstr ""

#: services-template.php:3, inc/post-types.php:48, inc/post-types.php:53
msgid "Services"
msgstr ""

#: inc/customizer.php:38
msgid "Theme Options"
msgstr ""

#: inc/customizer.php:54, inc/customizer.php:101
msgid "Lazy-load images"
msgstr ""

#: inc/customizer.php:57
msgid "Lazy-loading images means images are loaded only when they are in view. Improves performance, but can result in content jumping around on slower connections."
msgstr ""

#: inc/customizer.php:59
msgid "Lazy-load on (default)"
msgstr ""

#: inc/customizer.php:60
msgid "Lazy-load off"
msgstr ""

#: inc/customizer.php:102
msgid "Load images immediately"
msgstr ""

#: inc/post-types.php:15, inc/post-types.php:20
msgid "Doctors"
msgstr ""

#: inc/post-types.php:16
msgid "Doctor"
msgstr ""

#: inc/post-types.php:49
msgid "Service"
msgstr ""

#. translators: %s: search query.
#: inc/template-tags.php:65
msgid "Search Results for: %s"
msgstr ""

#. translators: %s: post date.
#: inc/template-tags.php:101
msgctxt "post date"
msgid "Posted on %s"
msgstr ""

#. translators: %s: post author.
#: inc/template-tags.php:115
msgctxt "post author"
msgid "by %s"
msgstr ""

#. translators: used between list items, there is a space after the comma
#: inc/template-tags.php:131
msgid ", "
msgstr ""

#. translators: 1: list of categories.
#: inc/template-tags.php:134
msgid "Posted in %1$s"
msgstr ""

#. translators: used between list items, there is a space after the comma
#: inc/template-tags.php:148
msgctxt "list item separator"
msgid ", "
msgstr ""

#. translators: 1: list of tags.
#: inc/template-tags.php:151
msgid "Tagged %1$s"
msgstr ""

#. translators: %s: post title
#: inc/template-tags.php:166
msgid "Leave a Comment<span class=\"screen-reader-text\"> on %s</span>"
msgstr ""

#. translators: %s: Name of current post. Only visible to screen readers
#: inc/template-tags.php:188
msgid "Edit <span class=\"screen-reader-text\">%s</span>"
msgstr ""

#. translators: %s: original post where attachment was added.
#: inc/template-tags.php:266
msgctxt "original post"
msgid "in %s"
msgstr ""

#: inc/template-tags.php:282
msgid "Post navigation"
msgstr ""

#: inc/template-tags.php:286
msgid "Previous attachment:"
msgstr ""

#: inc/template-tags.php:292
msgid "Next attachment:"
msgstr ""

#: template-parts/content-none.php:13
msgid "Nothing Found"
msgstr ""

#. translators: 1: link to WP admin new post page.
#: template-parts/content-none.php:26
msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: template-parts/content-none.php:41
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr ""

#: template-parts/content-none.php:49
msgid "It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help."
msgstr ""

#: template-parts/content-page.php:25, template-parts/content.php:56
msgid "Pages:"
msgstr ""

#. translators: %s: Name of current post. Only visible to screen readers
#: template-parts/content.php:43
msgid "Continue reading<span class=\"screen-reader-text\"> \"%s\"</span>"
msgstr ""

#: template-parts/content.php:76
msgid "Previous:"
msgstr ""

#: template-parts/content.php:77
msgid "Next:"
msgstr ""

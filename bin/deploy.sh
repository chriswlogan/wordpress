#!/usr/bin/env bash

deploy() {
	local user="$1"
	local pass="$2"
	local host="$3"
	local path="$4"
	local project_dir="$5"
	local excludes=""

	command -v lftp >/dev/null 2>&1 || {
		echo >&2 "lftp not installed. Aborting.";
		exit 1;
	}

	if [ -f '.deployignore' ]; then
		excludes=$(sed 's/^/--exclude /' .deployignore | tr '\n' ' ')
	fi

	lftp "sftp://${user}:${pass}@${host}" -e "set sftp:auto-confirm yes; mirror -a -c -e -R --use-cache --verbose --parallel=10 ${project_dir} ${path} ${excludes};"
}

deploy "$@"
